import React from 'react'

const NotFound = (props) => {
    return (
        <div className="ui grid middle aligned segment red inverted">
            <div className="ui column center aligned">
                <div className="ui inverted statistic">
                    <div className="value">404</div>
                    <div className="label">NOT FOUND</div>
                </div>
                <div>
                    <button className="ui white button" onClick={() => {
                        props.history.push("/")
                    }}>To Main
                    </button>
                </div>
            </div>
        </div>)
}

export default NotFound;

