import React from 'react'

const ServerError = (props) => {
    return (
        <div className="ui grid middle aligned segment blue inverted">
            <div className="ui column center aligned">
                <div className="ui inverted statistic">
                    <div className="value">5xx</div>
                    <div className="label">SERVER ERROR</div>
                </div>
                <div>
                    <button className="ui white button" onClick={() => {
                        props.history.push("/")
                    }}>To Main
                    </button>
                </div>
            </div>
        </div>)
}

export default ServerError;

