import {combineReducers} from "redux";
import ChatReducer from './chat/reducer'
import LoginReducer from './logIn/reducer'

const rootReducers = combineReducers({
    ChatReducer,
    LoginReducer

})

export default rootReducers;