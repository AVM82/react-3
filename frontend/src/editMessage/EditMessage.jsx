import React from "react";
import './EditMessage.css'
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {hidePage, saveMessage} from "./Action";
import {convertDate} from "../service/service";

class EditMessage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            text: ''
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleSave = this.handleSave.bind(this);
    }

    componentWillReceiveProps(nextProps, any) {
        if (nextProps.message.text !== this.props.message.text) {
            this.setState({
                text: nextProps.message.text
            })
        }
    }

    getPageContent() {
        return (
            <div className={'modal-overlay'}>
                <div className={'dialog-wrp'}>
                    <div>
                        <textarea className={'text-area'}
                                  onChange={this.handleChange}
                                  value={this.state.text}/>
                    </div>
                    <div className={'button-group'}>
                        <button onClick={this.handleClose} className="ui button">
                            Cancel
                        </button>
                        <button onClick={this.handleSave} className="ui primary button">
                            Save
                        </button>
                    </div>
                </div>
            </div>
        );
    }

    handleChange(event) {
        this.setState({
            text: event.target.value
        })
    }


    render() {

        const isShown = this.props.isShown
        return isShown ? this.getPageContent() : null;
    }

    handleClose() {
        this.props.hidePage();
    }

    handleSave() {
        const date = new Date();
        let data = {
            id: this.props.message.id,
            userId: this.props.user.id,
            avatar: this.props.user.avatar,
            user: this.props.user.login,
            text: this.state.text,
            createdAt: this.props.message.createdAt,
            editedAt: convertDate(date)
        };
        this.props.saveMessage(data);
    }

}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        hidePage: hidePage,
        saveMessage: saveMessage
    }, dispatch)

}

function mapStateToProps(state) {
    return {
        isShown: state.ChatReducer.showEdit,
        message: state.ChatReducer.editMessage,
        messages: state.ChatReducer.messages,
        user: state.ChatReducer.user
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditMessage);
