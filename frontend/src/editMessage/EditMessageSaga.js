import {call, put, takeLatest} from "@redux-saga/core/effects";
import axios from "axios";
import {API_MESSAGES} from "../api";
import {getMessage} from "../chat/action";
import {SAVE_MESSAGE} from "./actionType";
import {hidePage} from "./Action";

function* workerEditMessage(action) {
    try {
        yield call(axios.put, `${API_MESSAGES}`, action.payload);
        yield put(hidePage());
        yield put(getMessage());
    } catch (e) {
        yield put((e.message));
    }
}

export function* watchEditMessage() {
    yield takeLatest(SAVE_MESSAGE, workerEditMessage);
}