import React from 'react';
import {Redirect, Route} from 'react-router-dom';

const AdminRoute = ({component: Component, store, ...rest}) => {

    const role = localStorage.getItem('role');
    return (
        <Route {...rest} render={props => (

            role === 'admin' ? <Component {...props} /> : <Redirect to="/401"/>
        )}/>
    );
};

export default AdminRoute;
