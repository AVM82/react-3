import React from "react";
import '../spinner/Spinner.css'
import './MyMessage.css'
import {getDateTimeMessage} from "../service/service";

class MyMessage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showEditMessage: false
        }

        this.handleDeleteMessage = this.handleDeleteMessage.bind(this);
        this.handleEditMessage = this.handleEditMessage.bind(this);
    }

    render() {
        return (
            <>
                {this.props.data.divider}
                <div className='message-row'>
                    <div id='mes' className='my-message-wrp'>
                        <div className='message'>
                            <div className='header-message'>
                                <span>{this.props.data.message.user}</span>
                                <span>{getDateTimeMessage(new Date(this.props.data.message.createdAt).getTime())}</span>
                            </div>
                            <div className='text-message'>
                                <span>{this.props.data.message.text}</span>
                            </div>
                        </div>
                    </div>
                    <div id='menu' className='my-user-menu'>
                        <label id='edit' onClick={this.handleEditMessage}><i className="edit icon"/></label>
                        <label onClick={this.handleDeleteMessage} className='trash'><i
                            className="trash alternate icon"/></label>
                    </div>
                </div>
            </>
        );
    }

    handleDeleteMessage() {
        const uuid = this.props.data.message.id;
        this.props.deleteMessage(uuid);
    }

    handleEditMessage() {
        const message = this.props.data.message;
        this.props.editMessage(message);
    }
}

export default MyMessage
