import {call, put, takeLatest} from "@redux-saga/core/effects";

import axios from "axios";
import {API_USER} from "../api";
import {LOGIN} from "./actionType";
import {authorized, setUser, unauthorized} from "./action";


function* workerLogin(action) {
    try {
        const data = yield call(axios.post, `${API_USER}/login`, action.payload);
        yield localStorage.setItem('role', data.data.role);
        yield localStorage.setItem('auth', 'true');
        yield put(setUser(data.data));
        yield put(authorized(data.data));
    } catch (e) {
        yield put(unauthorized(e.message));
        yield localStorage.setItem('auth', 'false');
    }
}

export function* watchLogin() {
    yield takeLatest(LOGIN, workerLogin);
}