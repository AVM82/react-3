import * as React from "react";
import './LogIn.css'
import * as PropTypes from "prop-types";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {login} from "./action";
import Spinner from "../spinner/Spinner";
import {Redirect} from "react-router";


class LogIn extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            password: ''
        }
        this.handleLogIn = this.handleLogIn.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    render() {
        if (this.props.loading) return <Spinner/>;
        if (this.props.redirect !== '') {
            return <Redirect from={'/'} to={this.props.redirect}/>;
        }
        return (
            <div className="ui middle aligned center aligned">
                <div className="column">
                    <h2 className="title">
                        Log-in to your account
                    </h2>
                    <form className="ui large form">
                        <div className="ui stacked segment">
                            <div className="field">
                                <div className="ui left icon input">
                                    <i className="user icon"/>
                                    <input
                                        onChange={this.handleChange}
                                        value={this.state.name}
                                        type="text"
                                        name="name"
                                        placeholder="Login"/>
                                </div>
                            </div>
                            <div className="field">
                                <div className="ui left icon input">
                                    <i className="lock icon"/>
                                    <input
                                        onChange={this.handleChange}
                                        value={this.state.password}
                                        type="password"
                                        name="password"
                                        placeholder="Password"/>
                                </div>
                            </div>
                            {(this.props.unauthorized) ? <div className="error">{this.props.error}</div> : null}
                            <div className="ui fluid large teal submit button" onClick={this.handleLogIn}>Login</div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }

    handleLogIn() {
        this.props.login({
            login: this.state.name,
            password: this.state.password
        })
    }

    handleChange(event) {
        if (event.target.name === "name") {
            this.setState({
                name: event.target.value
            })
        } else if (event.target.name === "password") {
            this.setState({
                password: event.target.value
            })
        }

    }
}

LogIn.propTypes = {
    name: PropTypes.string,
    password: PropTypes.string
};


function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        login: login
    }, dispatch)
}

function mapStateToProps(state) {
    return {
        loading: state.LoginReducer.loading,
        unauthorized: state.LoginReducer.unauthorized,
        user: state.LoginReducer.user,
        redirect: state.LoginReducer.redirect,
        error: state.LoginReducer.error
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LogIn);


