import {AUTHORIZED, LOGIN, SET_USER, UNAUTHORIZED} from "./actionType";


export const login = (data) => {
    return {
        type: LOGIN,
        payload: data
    }
}

export const authorized = (data) => {
    return {
        type: AUTHORIZED,
        payload: data
    }
}

export const setUser = (data) => {
    return {
        type: SET_USER,
        payload: data
    }
}

export const unauthorized = (data) => {
    return {
        type: UNAUTHORIZED,
        payload: data

    }
}
