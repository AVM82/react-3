import {AUTHORIZED, SERVER_ERROR, UNAUTHORIZED} from "./actionType";

const initialState = {
    loading: false,
    unauthorized: true,
    error: '',
    redirect: '',
    user: {}
};

export default function (state = initialState, action) {
    switch (action.type) {
        case AUTHORIZED: {
            return {
                ...state,
                loading: false,
                user: action.payload,
                unauthorized: false,
                redirect: '/chat'
            }
        }

        case UNAUTHORIZED: {
            return {
                ...state,
                user: {},
                loading: false,
                unauthorized: true,
                error: action.payload
            };

        }

        case SERVER_ERROR: {
            return {
                ...state,
                user: {},
                loading: false,
                unauthorized: true,
                redirect: '/5xx'
            };

        }

        default:
            return state;
    }
}
