import React from "react";
import './OtherMessage.css'
import {getDateTimeMessage} from "../service/service";

class OtherMessage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            like: false
        }

        this.handleLike = this.handleLike.bind(this);
    }

    render() {
        return (

            <>
                {this.props.data.divider}
                <div className='other-mes-row'>
                    <div className='message-wrp'>
                        <div className='avatar'>
                            <img className="ui medium circular image" src={this.props.data.message.avatar} alt=''/>
                            <img className="ui medium circular image" src='' alt=''/>
                        </div>
                        <div className='message'>
                            <div className='header-message'>
                                <span>{this.props.data.message.user}</span>
                                <span>{getDateTimeMessage(new Date(this.props.data.message.createdAt).getTime())}</span>
                            </div>
                            <div className='text-message'>
                                <span>{this.props.data.message.text}</span>
                            </div>
                        </div>

                    </div>
                    <div className='heart-wrp'>
                        {this.state.like ?
                            <a href="/#"><i onClick={this.handleLike} className="heart icon"/></a> :
                            <a href="/#"><i onClick={this.handleLike} className="heart outline icon"/></a>}
                    </div>
                </div>
            </>
        );
    }


    handleLike() {
        this.setState({
            like: !this.state.like
        })
    }

}

export default OtherMessage