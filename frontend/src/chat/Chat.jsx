import React from 'react';
import './Chat.css';
import Header from "../header/Header";
import Spinner from "../spinner/Spinner";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {deleteMessage, getMessage} from "./action";
import MyMessage from "../myMessage/MyMessage";
import OtherMessage from "../otherMessage/OtherMessage";
import {convertToOnlyDate} from "../service/service";
import SendMessage from "../sendMessage/SendMessage";
import EditMessage from "../editMessage/EditMessage";
import {showPage} from "../editMessage/Action";


class Chat extends React.Component {

    constructor(props) {
        super(props);

        this.editMessage = this.editMessage.bind(this);

    }


    componentDidMount() {
        this.props.getMessage();
    }

    editMessage(message) {
        this.props.showPage(message);
    }

    render() {
        if (this.props.loading) return <Spinner/>;
        let messageDate = undefined;
        return (
            <>
                <div className='chat-wrp'>
                    <EditMessage/>
                    <Header data={
                        {
                            uniqueUser: this.props.uniqueUser,
                            countMes: this.props.messages.length,
                            lastMessage: this.props.lastMessageDateTime
                        }
                    }/>

                    <div className='main-wrp'>
                        <div className='message-list-wrp'>
                            {
                                this.props.messages.map((item, index) => {
                                    let divider;
                                    let currentMesDate = convertToOnlyDate(new Date(item.createdAt).getTime())
                                    if (messageDate !== currentMesDate) {
                                        messageDate = currentMesDate;
                                        divider = <div className="ui horizontal divider">{currentMesDate}</div>
                                    } else {
                                        divider = null;
                                    }
                                    return (
                                        (item.userId === this.props.user.id) ?
                                            <MyMessage data={{
                                                message: item,
                                                divider: divider
                                            }}
                                                       editMessage={this.editMessage}
                                                       deleteMessage={this.props.deleteMessage}
                                                       key={index}/> :
                                            <OtherMessage data={{
                                                message: item,
                                                divider: divider
                                            }}
                                                          key={index}/>)
                                })
                            }
                            <div className='hack-padding'
                                 ref={(el) => {
                                     this.messagesEnd = el;
                                 }}>
                            </div>
                        </div>
                    </div>
                    <SendMessage/>
                </div>
            </>
        )
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getMessage: getMessage,
        showPage: showPage,
        deleteMessage: deleteMessage
    }, dispatch)

}

function mapStateToProps(state) {
    return {
        messages: state.ChatReducer.messages,
        loading: state.ChatReducer.loading,
        uniqueUser: state.ChatReducer.uniqueUser,
        lastMessageDateTime: state.ChatReducer.lastMessage,
        user: state.ChatReducer.user
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
