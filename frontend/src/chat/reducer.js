import {ADD_MESSAGE, FETCH_MESSAGE} from "./actionType";
import {HIDE_PAGE, SHOW_PAGE} from "../editMessage/actionType";
import {SET_USER} from "../logIn/actionType";

const initialState = {
    loading: true,
    messages: [],
    lastMessage: '',
    uniqueUser: 0,
    showEdit: false,
    editMessage: {},
    isAuth: false,
    user: {}

};

export default function (state = initialState, action) {
    switch (action.type) {

        case FETCH_MESSAGE: {
            return {
                ...state,
                uniqueUser: action.payload.uniqueUser,
                lastMessage: action.payload.lastMessage,
                messages: action.payload.messages,
                loading: false
            };

        }

        case ADD_MESSAGE: {
            return {
                ...state,
                messages: [...state.messages, action.payload],
                lastMessage: action.payload.createdAt,
                loading: true
            };

        }

        case SET_USER: {
            return {
                ...state,
                user: action.payload,
                isAuth: true
            };

        }

        case SHOW_PAGE: {


            return {
                ...state,
                showEdit: action.payload.showEdit,
                editMessage: action.payload.message
            };

        }

        case HIDE_PAGE: {
            return {
                ...state,
                showEdit: action.payload
            };

        }

        default:
            return state;
    }
}
