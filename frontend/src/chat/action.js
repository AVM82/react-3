import {DELETE_MESSAGE, FETCH_MESSAGE, LOAD_MESSAGE} from "./actionType";
import {convertDate, getUniqueUser} from "../service/service";

export const getMessage = () => {
    return {
        type: LOAD_MESSAGE
    }
}

export const putMessages = (data) => {

    const count = data.length;
    const uniqueUser = getUniqueUser(data);
    const lastMesDateTime = convertDate(new Date(data[count - 1].createdAt).getTime())
    return (
        {
            type: FETCH_MESSAGE,
            payload: {
                messages: data,
                uniqueUser: uniqueUser.length,
                lastMessage: lastMesDateTime
            }
        }
    )
}

export const deleteMessage = (uuid) => {
    return {
        type: DELETE_MESSAGE,
        payload: uuid
    }

}
