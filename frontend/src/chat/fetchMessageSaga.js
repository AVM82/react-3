import {DELETE_MESSAGE, LOAD_MESSAGE} from "./actionType";
import {call, put, takeLatest} from "@redux-saga/core/effects";
import {getMessageApi} from "../service/api";
import {getMessage, putMessages} from "./action";
import axios from "axios";
import {API_MESSAGES} from "../api";


function* workerLoadMessage() {

    const data = yield call(getMessageApi);
    yield put(putMessages(data))
}


export function* watchFetchMessage() {
    yield takeLatest(LOAD_MESSAGE, workerLoadMessage);
}


function* workerDeleteMessage(action) {

    try {
        yield call(axios.delete, `${API_MESSAGES}/${action.payload}`);
        yield put(getMessage());
    } catch (e) {
        yield put((e.message));
    }
}


export function* watchDeleteMessage() {
    yield takeLatest(DELETE_MESSAGE, workerDeleteMessage);
}