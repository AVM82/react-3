import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'semantic-ui-css/semantic.min.css'
import {Provider} from "react-redux";
import {applyMiddleware, createStore} from "redux";
import rootReducers from "./rootReducers";
import {composeWithDevTools} from "redux-devtools-extension";
import thunk from "redux-thunk";
import createSagaMiddleware from "redux-saga"
import logger from "redux-logger"
import rootSaga from "./sagas";
import LogIn from "./logIn/LogIn";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import NotFound from "./error/NotFound";
import Chat from "./chat/Chat";
import ServerError from "./error/ServerError";
import Unauthorized from "./error/Unauthorized";
import PrivateRoute from "./PrivateRoute";
import UserList from "./userList/UserList";
import AdminRoute from "./AdminRoute";

const sagaMiddleware = createSagaMiddleware();

const store = createStore(rootReducers, composeWithDevTools(applyMiddleware(logger, sagaMiddleware, thunk)));

sagaMiddleware.run(rootSaga);


ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <BrowserRouter>
                <Switch>
                    <Route exact path="/" component={LogIn}/>
                    <PrivateRoute
                        exact
                        path='/chat'
                        component={Chat}
                    />
                    <AdminRoute
                        path='/users'
                        component={UserList}
                    />
                    <Route exact path="/5xx" component={ServerError}/>
                    <Route exact path="/401" component={Unauthorized}/>
                    <Route path="*" component={NotFound}/>
                </Switch>
            </BrowserRouter>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);

