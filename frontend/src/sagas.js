import {all} from "@redux-saga/core/effects";
import {watchDeleteMessage, watchFetchMessage} from "./chat/fetchMessageSaga";
import {watchLogin} from "./logIn/loginSaga";
import {watchSendMessage} from "./sendMessage/SendMessageSaga";
import {watchEditMessage} from "./editMessage/EditMessageSaga";

export default function* rootSaga() {
    yield all([
        watchFetchMessage(),
        watchLogin(),
        watchSendMessage(),
        watchDeleteMessage(),
        watchEditMessage()

    ])
}