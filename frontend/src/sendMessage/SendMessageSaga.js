import {call, put, takeLatest} from "@redux-saga/core/effects";

import axios from "axios";
import {API_MESSAGES} from "../api";
import {SEND_MESSAGE} from "./ActionType";
import {getMessage} from "../chat/action";


function* workerSendMessage(action) {
    try {
        yield call(axios.post, `${API_MESSAGES}`, action.payload);
        yield put(getMessage());
    } catch (e) {
        yield put((e.message));
    }
}

export function* watchSendMessage() {
    yield takeLatest(SEND_MESSAGE, workerSendMessage);
}