import React from "react";
import './SendMessage.css'
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {sendMessage} from "./Action";
import {convertDate} from "../service/service";

class SendMessage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: ''
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    render() {
        return (
            <div className='message-input-wrp'>
                <div className='message-input'>
                    <input
                        onChange={this.handleChange}
                        value={this.state.text}
                        placeholder="Type your message here"
                    />
                    <div className='send-button'>
                        <button onClick={this.handleSubmit} className="ui circular twitter icon button">
                            <i className="share icon"/>
                        </button>
                    </div>
                </div>
            </div>

        );
    }

    handleChange(event) {
        this.setState({
            text: event.target.value
        })
    }

    handleSubmit(event) {
        if (this.state.text === '') return;
        event.preventDefault();
        const date = new Date();
        this.props.sendMessage({
            id: '',
            userId: this.props.user.id,
            avatar: this.props.user.avatar,
            user: this.props.user.login,
            text: this.state.text,
            createdAt: convertDate(date),
            editedAt: ''
        })
        this.setState({
            text: ''
        })
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        sendMessage: sendMessage
    }, dispatch)
}

function mapStateToProps(state) {
    return {
        user: state.ChatReducer.user

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SendMessage);