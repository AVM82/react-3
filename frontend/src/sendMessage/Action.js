import {SEND_MESSAGE} from "./ActionType";

export const sendMessage = (data) => {
    return {
        type: SEND_MESSAGE,
        payload: data
    }
}