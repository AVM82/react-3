import {API_MESSAGES} from "../api";


export const getMessageApi = () => {
    return fetch(API_MESSAGES)
        .then((res) => res.json())
        .then((res) => {
            return res.sort((a, b) => {
                const first = new Date(a.createdAt).getTime();
                const second = new Date(b.createdAt).getTime();
                if (first > second) {
                    return 1;
                } else if (first < second) {
                    return -1;
                }
                return 0;
            })
        })
        .catch((error) => {
            console.log(`😱 Request failed: ${error}`);
        });
}