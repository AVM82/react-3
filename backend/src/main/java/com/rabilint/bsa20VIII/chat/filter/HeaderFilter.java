package com.rabilint.bsa20VIII.chat.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class HeaderFilter implements Filter {

    private static final Logger logger = LoggerFactory.getLogger(HeaderFilter.class);

    @Override
    public void init(final FilterConfig filterConfig) {
        logger.info("Initializing bsa.java.concurrency.filter :{}", this);
    }

    @Override
    public void doFilter(ServletRequest request,
                         ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        logger.info(
                "Logging Request  {} : {} : {}", req.getMethod(),
                req.getRequestURI(),
                request.getServerName());
        chain.doFilter(request, response);
        logger.info(
                "Logging Response {}:{}",
                res.getContentType(),
                res.getStatus());
    }


    @Override
    public void destroy() {
        logger.warn("Destructing bsa.java.concurrency.filter :{}", this);
    }

}
