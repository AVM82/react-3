package com.rabilint.bsa20VIII.chat.repository;

import com.rabilint.bsa20VIII.chat.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    User getById(String id);

    Optional<User> findByLoginAndPassword(String login, String password);

}
