package com.rabilint.bsa20VIII.chat.controller;

import com.rabilint.bsa20VIII.chat.dto.UserDTO;
import com.rabilint.bsa20VIII.chat.dto.UserLoginDTO;
import com.rabilint.bsa20VIII.chat.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping()
    public List<UserDTO> getAll() {
        return userService.getAll();

    }

    @GetMapping("/{id}")
    public UserDTO getById(@PathVariable String id) {
        return userService.getOneById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found"));

    }

    @PostMapping()
    public String createUser(@RequestBody UserDTO userDTO) {
        return userService.createUser(userDTO)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Can not create user."));

    }

    @PostMapping("/login")
    public UserDTO logIn(@RequestBody UserLoginDTO userLoginDTO) {
        return userService.logIn(userLoginDTO)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User not found"));
    }
}
