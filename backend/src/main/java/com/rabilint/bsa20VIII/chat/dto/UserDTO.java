package com.rabilint.bsa20VIII.chat.dto;

import com.rabilint.bsa20VIII.chat.entity.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {
    private String id;
    private String login;
    private String password;
    private String role;
    private String avatar;

    public static UserDTO fromEntity(User user) {
        return UserDTO
                .builder()
                .id(user.getId())
                .login(user.getLogin())
                .password(user.getPassword())
                .role(user.getRole())
                .avatar(user.getAvatar())
                .build();
    }
}
