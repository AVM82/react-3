package com.rabilint.bsa20VIII.chat.repository;

import com.rabilint.bsa20VIII.chat.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageRepository extends JpaRepository<Message, String> {
}
