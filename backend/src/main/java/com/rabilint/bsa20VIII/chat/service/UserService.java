package com.rabilint.bsa20VIII.chat.service;

import com.rabilint.bsa20VIII.chat.dto.UserDTO;
import com.rabilint.bsa20VIII.chat.dto.UserLoginDTO;
import com.rabilint.bsa20VIII.chat.entity.User;
import com.rabilint.bsa20VIII.chat.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserDTO> getAll() {
        return userRepository.findAll()
                .stream()
                .map(UserDTO::fromEntity)
                .collect(Collectors.toList());
    }

    public Optional<UserDTO> getOneById(String id) {
        return userRepository.findById(id)
                .map(UserDTO::fromEntity);
    }

    public Optional<String> createUser(UserDTO userDTO) {
        try {
            var user = User.fromDTO(userDTO);
            var result = userRepository.save(user);
            return Optional.of(result.getId());
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    public Optional<UserDTO> logIn(UserLoginDTO userLoginDTO) {
        return userRepository.findByLoginAndPassword(userLoginDTO.getLogin(), userLoginDTO.getPassword())
                .map(UserDTO::fromEntity);
    }
}
