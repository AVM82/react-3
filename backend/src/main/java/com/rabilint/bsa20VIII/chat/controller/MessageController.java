package com.rabilint.bsa20VIII.chat.controller;

import com.rabilint.bsa20VIII.chat.dto.MessageDTO;
import com.rabilint.bsa20VIII.chat.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/messages")
public class MessageController {

    @Autowired
    MessageService messageService;

    @GetMapping()
    public List<MessageDTO> getAll() {
        return messageService.getAll();

    }

    @PostMapping()
    public String createMessage(@RequestBody MessageDTO messageDTO) {
        return messageService.createMessage(messageDTO)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Can not create message."));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteMessage(@PathVariable("id") String messageId) {
        messageService.deleteImage(messageId);

    }

    @PutMapping()
    String updateMessage(@RequestBody MessageDTO messageDTO) {
        return messageService.createMessage(messageDTO)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Can not create message."));
    }
}
