package com.rabilint.bsa20VIII.chat;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabilint.bsa20VIII.chat.dto.MessageDTO;
import com.rabilint.bsa20VIII.chat.entity.Message;
import com.rabilint.bsa20VIII.chat.entity.User;
import com.rabilint.bsa20VIII.chat.repository.MessageRepository;
import com.rabilint.bsa20VIII.chat.repository.UserRepository;
import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@SpringBootApplication
public class ChatApplication {


    public static void main(String[] args) throws JSONException {
        SpringApplication.run(ChatApplication.class, args);
    }

    private static String getMes() {
        final HttpClient client = HttpClient.newHttpClient();
        try {
            var response = client.send(buildGetRequest(), HttpResponse.BodyHandlers.ofString());
            return response.body();
        } catch (IOException | InterruptedException ex) {
            return "";
        }
    }

    private static HttpRequest buildGetRequest() {
        return HttpRequest
                .newBuilder()
                .uri(URI.create("https://edikdolynskyi.github.io/react_sources/messages.json"))
                .GET()
                .build();
    }

    @Bean
    CommandLineRunner initDatabase(MessageRepository messageRepository, UserRepository userRepository) {

        return args -> {
            ObjectMapper mapper = new ObjectMapper();
            JSONArray array = new JSONArray(getMes());
            MessageDTO messageDTO;
            try {
                for (int i = 0; i < array.length(); i++) {
                    messageDTO = mapper.readValue(array.get(i).toString(), MessageDTO.class);
                    User user = userRepository.getById(messageDTO.getUserId());
                    messageRepository.save(Message.fromDto(messageDTO, user));
                }
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        };
    }
}
