package com.rabilint.bsa20VIII.chat.dto;

import com.rabilint.bsa20VIII.chat.entity.Message;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MessageDTO {
    private String id;
    private String userId;
    private String avatar;
    private String user;
    private String text;
    private String createdAt;
    private String editedAt;

    public static MessageDTO fromEntity(Message message) {
        return MessageDTO.builder()
                .id(message.getId())
                .userId(message.getUser().getId())
                .avatar(message.getUser().getAvatar())
                .user(message.getUser().getLogin())
                .text(message.getText())
                .createdAt(message.getCreatedAt())
                .editedAt(message.getEditedAt())
                .build();
    }
}
