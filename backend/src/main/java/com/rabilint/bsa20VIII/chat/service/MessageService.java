package com.rabilint.bsa20VIII.chat.service;

import com.rabilint.bsa20VIII.chat.dto.MessageDTO;
import com.rabilint.bsa20VIII.chat.entity.Message;
import com.rabilint.bsa20VIII.chat.repository.MessageRepository;
import com.rabilint.bsa20VIII.chat.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MessageService {

    @Autowired
    MessageRepository messageRepository;

    @Autowired
    UserRepository userRepository;

    public List<MessageDTO> getAll() {
        List<Message> all = messageRepository.findAll();
        return all
                .stream()
                .map(MessageDTO::fromEntity)
                .collect(Collectors.toList());
    }

    public Optional<String> createMessage(MessageDTO messageDTO) {
        try {
            var user = userRepository.getById(messageDTO.getUserId());
            if (messageDTO.getId().isEmpty()) {
                messageDTO.setId(String.valueOf(UUID.randomUUID()));
            }
            var message = Message.fromDto(messageDTO, user);
            var result = messageRepository.save(message);
            return Optional.of(result.getId());
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    public void deleteImage(String messageId) {
        messageRepository.deleteById(messageId);
    }
}
