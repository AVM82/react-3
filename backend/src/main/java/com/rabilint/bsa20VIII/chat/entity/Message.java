package com.rabilint.bsa20VIII.chat.entity;


import com.rabilint.bsa20VIII.chat.dto.MessageDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "messages")
public class Message {

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    private String id;

    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    @JoinColumn(name = "userId")
    private User user;

    @Column
    private String text;

    @Column
    private String createdAt;

    @Column
    private String editedAt;

    public static Message fromDto(MessageDTO messageDTO, User user) {
        return Message.builder()
                .id(messageDTO.getId())
                .text(messageDTO.getText())
                .createdAt(messageDTO.getCreatedAt())
                .editedAt(messageDTO.getEditedAt())
                .user(user)
                .build();
    }

}
