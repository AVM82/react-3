insert into users (id, login, password, role, avatar)
values (random_uuid(), 'admin', 'admin', 'admin', '');

insert into users (id, login, password, role, avatar)
values ('9e243930-83c9-11e9-8e0c-8f1a686f4ce4', 'Ruth', 'Ruth', 'user',
        'https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA');

insert into users (id, login, password, role, avatar)
values ('533b5230-1b8f-11e8-9629-c7eca82aa7bd', 'Wendy', 'Wendy', 'user',
        'https://resizing.flixster.com/EVAkglctn7E9B0hVKJrueplabuQ=/220x196/v1.cjs0NjYwNjtqOzE4NDk1OzEyMDA7MjIwOzE5Ng');

insert into users (id, login, password, role, avatar)
values ('4b003c20-1b8f-11e8-9629-c7eca82aa7bd', 'Helen', 'Helen', 'user',
        'https://resizing.flixster.com/PCEX63VBu7wVvdt9Eq-FrTI6d_4=/300x300/v1.cjs0MzYxNjtqOzE4NDk1OzEyMDA7MzQ5OzMxMQ');

insert into users (id, login, password, role, avatar)
values ('5328dba1-1b8f-11e8-9629-c7eca82aa7bd', 'Ben', 'Ben', 'user',
        'https://www.aceshowbiz.com/images/photo/tom_pelphrey.jpg');

